function problem5(inventory,problem4)
{
    if(Array.isArray(inventory) && inventory.length != 0 && Array.isArray(problem4) && problem4.length!=0)
    {
    let olderYearCar = [];
    for(let i in inventory)
    {
        if(problem4[i] > 2000)
        {
            olderYearCar.push(inventory[i].car_year);
        }
    }
    return olderYearCar;
}
else{
    return [];
}
}
module.exports=problem5;