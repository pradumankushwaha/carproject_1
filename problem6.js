function problem6(inventory)
    {
        if(Array.isArray(inventory) && inventory.length != 0)
         {
             let bmwAudi = [];
             for(let i of inventory)
             {
                 if(i.car_make === "Audi" || i.car_make === "BMW")
                 {
                     bmwAudi.push(i);
                 }
             }
             return JSON.stringify(bmwAudi);
         
         }else
         {
         return [];
         }
     }
module.exports=problem6;