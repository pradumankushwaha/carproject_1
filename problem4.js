function problem4(inventory){
    if(Array.isArray(inventory) && inventory.length !=0)
    {
        let year = [];
        for(let i in inventory){
            year.push(inventory[i].car_year)
        }
        return year;
    }else{
        return [];
    }
}
module.exports=problem4;